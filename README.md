<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/photo.jpg"><img src="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/photo.jpg" alt="截屏" style="max-width: 100%;"></a></p>
<p align="center" dir="auto">
    <a href="https://github.com/mickael-kerjean/contributors" alt="贡献者">
        <img src="https://camo.githubusercontent.com/599b79530207ac494c28171ef791f99e825bd9db5cdd0f636c802bbd9775a251/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f636f6e7472696275746f72732f6d69636b61656c2d6b65726a65616e2f66696c657374617368" data-canonical-src="https://img.shields.io/github/contributors/mickael-kerjean/filestash" style="max-width: 100%;">
    </a>
    <a href="https://hub.docker.com/r/machines/filestash" alt="Docker 中心" rel="nofollow">
        <img src="https://camo.githubusercontent.com/9424e225693e20c1a134feb8a46b7454e6608988e590bb86549cc6444a083acb/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f70756c6c732f6d616368696e65732f66696c657374617368" data-canonical-src="https://img.shields.io/docker/pulls/machines/filestash" style="max-width: 100%;">
    </a>
    <a href="https://kiwiirc.com/nextclient/#irc://irc.libera.chat/#filestash?nick=guest??" alt="在 IRC 上聊天" rel="nofollow">
        <img src="https://camo.githubusercontent.com/e4c9fe15646dfb4e980250d4a115e444ad24f79a63d64db57c34fd536936800b/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4952432d25323366696c6573746173682d627269676874677265656e2e737667" data-canonical-src="https://img.shields.io/badge/IRC-%23filestash-brightgreen.svg" style="max-width: 100%;">
    </a>
</p>
<p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
    类似 Dropbox 的文件管理器，可让您在任何地方管理数据：</font></font><br>
    <a href="https://www.filestash.app/ftp-client.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FTP</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> • FTPS • </font></font><a href="https://www.filestash.app/ssh-file-transfer.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SFTP</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> • </font></font><a href="https://www.filestash.app/webdav-client.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebDAV</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> • Git • </font></font><a href="https://www.filestash.app/s3-browser.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">S3</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> • NFS • Samba • Artifactory • </font></font><a href="https://www.filestash.app/ldap-browser.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LDAP</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> • Mysql </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
       Storj • CardDAV • CalDAV • Backblaze B2 • </font></font><a href="https://www.filestash.app/s3-browser.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Minio</font></font></a> <br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 
               Dropbox • Google Drive
</font></font></p>
<p align="center" dir="auto">
    <a href="http://demo.filestash.app" rel="nofollow">
      <img src="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/button_demo.png" alt="演示按钮" style="max-width: 100%;">
    </a>
</p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h1><a id="user-content-features" class="anchor" aria-label="固定链接：功能" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过浏览器管理文件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">认证中间件连接各种用户来源</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">灵活的分享机制</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chromecast 支持图像、音乐和视频</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频播放器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频转码（mov、mkv、avi、mpeg 等）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图片查看器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像转码（尼康、佳能等公司的原始图像）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">照片管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">音频播放器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">共享链接是功能齐全的网络驱动器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">办公文档（docx、xlsx 等）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完整的 Org 模式客户端 (</font></font><a href="https://www.filestash.app/2018/05/31/release-note-v0.1/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">方便使用的</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">移动友好</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可定制</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">插件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超级快</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上传文件和文件夹</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载为 zip</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多个云提供商和协议，易于扩展</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">彩虹猫装载机</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速访问：经常访问的文件夹固定到主页</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Emacs、VIM 或 Sublime 键绑定</font></font><code>;)</code></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">搜索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.. 还有很多</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></h1><a id="user-content-documentation" class="anchor" aria-label="永久链接：文档" href="#documentation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://www.filestash.app/docs/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></a></li>
<li><a href="https://www.filestash.app/docs/install-and-upgrade/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></a></li>
<li><a href="https://www.filestash.app/docs/faq/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">常问问题</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">截图</font></font></h1><a id="user-content-screenshots" class="anchor" aria-label="永久链接：截图" href="#screenshots"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto">
    <animated-image data-catalyst=""><a href="https://demo.filestash.app" rel="nofollow" data-target="animated-image.originalLink" hidden="">
        <img src="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/navigation.gif" alt="导航的用户体验" style="max-width: 100%;" data-target="animated-image.originalImage" hidden="">
    </a>
      <span class="AnimatedImagePlayer" data-target="animated-image.player">
        <a data-target="animated-image.replacedLink" class="AnimatedImagePlayer-images" href="https://demo.filestash.app/" target="_blank">
          <span data-target="animated-image.imageContainer">
            <img data-target="animated-image.replacedImage" alt="导航的用户体验" class="AnimatedImagePlayer-animatedImage" src="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/navigation.gif">
          </span>
        </a>
        <button data-target="animated-image.imageButton" class="AnimatedImagePlayer-images" tabindex="-1"></button>
        <span class="AnimatedImagePlayer-controls" data-target="animated-image.controls">
          <button data-target="animated-image.playButton" class="AnimatedImagePlayer-button">
            <svg aria-hidden="true" focusable="false" class="octicon icon-play" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M4 13.5427V2.45734C4 1.82607 4.69692 1.4435 5.2295 1.78241L13.9394 7.32507C14.4334 7.63943 14.4334 8.36057 13.9394 8.67493L5.2295 14.2176C4.69692 14.5565 4 14.1739 4 13.5427Z">
            </path></svg>
            <svg aria-hidden="true" focusable="false" class="octicon icon-pause" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
              <rect x="4" y="2" width="3" height="12" rx="1"></rect>
              <rect x="9" y="2" width="3" height="12" rx="1"></rect>
            </svg>
          </button>
          <a data-target="animated-image.openButton" aria-label="在新窗口中打开" class="AnimatedImagePlayer-button" href="https://demo.filestash.app/" target="_blank">
            <svg aria-hidden="true" class="octicon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
            </svg>
          </a>
        </span>
      </span></animated-image>
</p>
<p align="center" dir="auto">
    <animated-image data-catalyst=""><a href="http://demo.filestash.app" rel="nofollow" data-target="animated-image.originalLink" hidden="">
        <img src="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/photo_management.gif" alt="媒体用户体验" style="max-width: 100%;" data-target="animated-image.originalImage" hidden="">
    </a>
      <span class="AnimatedImagePlayer" data-target="animated-image.player">
        <a data-target="animated-image.replacedLink" class="AnimatedImagePlayer-images" href="http://demo.filestash.app/" target="_blank">
          <span data-target="animated-image.imageContainer">
            <img data-target="animated-image.replacedImage" alt="媒体用户体验" class="AnimatedImagePlayer-animatedImage" src="https://raw.githubusercontent.com/mickael-kerjean/filestash_images/master/.assets/photo_management.gif">
          </span>
        </a>
        <button data-target="animated-image.imageButton" class="AnimatedImagePlayer-images" tabindex="-1"></button>
        <span class="AnimatedImagePlayer-controls" data-target="animated-image.controls">
          <button data-target="animated-image.playButton" class="AnimatedImagePlayer-button">
            <svg aria-hidden="true" focusable="false" class="octicon icon-play" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M4 13.5427V2.45734C4 1.82607 4.69692 1.4435 5.2295 1.78241L13.9394 7.32507C14.4334 7.63943 14.4334 8.36057 13.9394 8.67493L5.2295 14.2176C4.69692 14.5565 4 14.1739 4 13.5427Z">
            </path></svg>
            <svg aria-hidden="true" focusable="false" class="octicon icon-pause" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
              <rect x="4" y="2" width="3" height="12" rx="1"></rect>
              <rect x="9" y="2" width="3" height="12" rx="1"></rect>
            </svg>
          </button>
          <a data-target="animated-image.openButton" aria-label="在新窗口中打开" class="AnimatedImagePlayer-button" href="http://demo.filestash.app/" target="_blank">
            <svg aria-hidden="true" class="octicon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16">
              <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
            </svg>
          </a>
        </span>
      </span></animated-image>
</p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">核心思想</font></font></h1><a id="user-content-the-core-idea" class="anchor" aria-label="永久链接：核心思想" href="#the-core-idea"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Filestash 最初是为了解决 Dropbox 问题而提出的，它通过抽象存储方面来解决，以便您可以通过实现此接口“自带后端”：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>type IBackend interface {
	Init(params map[string]string, app *App) (IBackend, error) // constructor
	Ls(path string) ([]os.FileInfo, error)           // list files in a folder
	Cat(path string) (io.ReadCloser, error)          // download a file
	Mkdir(path string) error                         // create a folder
	Rm(path string) error                            // remove something
	Mv(from string, to string) error                 // rename something
	Save(path string, file io.Reader) error          // save a file
	Touch(path string) error                         // create a file
	LoginForm() Form                                 // dynamic form generation for the login
}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="type IBackend interface {
	Init(params map[string]string, app *App) (IBackend, error) // constructor
	Ls(path string) ([]os.FileInfo, error)           // list files in a folder
	Cat(path string) (io.ReadCloser, error)          // download a file
	Mkdir(path string) error                         // create a folder
	Rm(path string) error                            // remove something
	Mv(from string, to string) error                 // rename something
	Save(path string, file io.Reader) error          // save a file
	Touch(path string) error                         // create a file
	LoginForm() Form                                 // dynamic form generation for the login
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它已经发展成为插件，插件就像乐高积木，你可以把它们组装在一起，形成一个适合你的解决方案。你可以带来自己的身份提供者、自己的授权、自己的搜索等等。只要你想要什么，插件就有可能实现。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下是我们为了科学目的而提出的“文件系统作为框架”理念的一些非常规示例：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mysql 插件将数据库显示为文件夹，将表显示为子文件夹，将行显示为单个文件。打开文件（= 一行）时，将向用户显示一个从 DB 架构动态呈现的表单，即使不懂 SQL 的人也可以编辑该表单并将其保存回 mysql。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ldap 后端，您可以从中浏览 LDAP 目录并查看/编辑其中包含的记录。例如：</font></font><a href="https://demo.filestash.app/login?type=ldap&amp;hostname=ldap%3A%2F%2Fipa.demo1.freeipa.org&amp;bind_dn=uid%3Dadmin%2Ccn%3Dusers%2Ccn%3Daccounts%2Cdc%3Ddemo1%2Cdc%3Dfreeipa%2Cdc%3Dorg&amp;bind_password=Secret123&amp;base_dn=cn%3Daccounts%2Cdc%3Ddemo1%2Cdc%3Dfreeipa%2Cdc%3Dorg" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这个公共 ldap</font></font></a></li>
</ul>

<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></h1><a id="user-content-support" class="anchor" aria-label="固定链接：支持" href="#support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于公司 -&gt;</font></font><a href="https://www.filestash.app/pricing/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持合同</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于个人 -&gt; IRC (libera.chat) 上的</font></font><a href="https://kiwiirc.com/nextclient/#irc://irc.libera.chat/#filestash?nick=guest??" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">#filestash</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。若要为项目提供资金捐助：
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">比特币：</font></font><code>3LX5KGmSmHDj5EuXrmUvcg77EJxCxmdsgW</code></li>
<li><a href="https://opencollective.com/filestash" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放集体</font></font></a></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">致谢</font></font></h1><a id="user-content-credits" class="anchor" aria-label="永久链接：致谢" href="#credits"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/mickael-kerjean/filestash/graphs/contributors"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献者</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和开发</font></font><a href="https://github.com/mickael-kerjean/filestash/blob/master/go.mod"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">出色库的人员</font></font></a></li>
<li><font style="vertical-align: inherit;"></font><a href="https://github.com/ssnjrthegr8"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">徽标源自ssnjrthegr8</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的作品</font><font style="vertical-align: inherit;">，图像来自</font></font><a href="https://www.flaticon.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">flaticon</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://fontawesome.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">fontawesome</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://material.io/icons/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">material</font></font></a></li>
<li><a href="https://github.com/libvips/libvips"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">libvips</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://github.com/LibRaw/LibRaw"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">libraw</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。这些库在 Filestash 中静态编译。构建 Filestash 的说明可</font></font><a href="https://github.com/mickael-kerjean/filestash/blob/master/.drone.yml"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找到，为 libvips 和 libraw 创建自己的静态库的说明可</font></font><a href="https://github.com/mickael-kerjean/filestash/tree/master/server/plugin/plg_image_light/deps"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此处找到</font></font></a></li>
</ul>
</article></div>
